##############################################################################
#IMPORT
import pandas as pd
import time
from selenium import webdriver

##############################################################################
#FUNCIONES
def wait_element(driver, element, counter):
    while(counter > 0):
        try:
            driver.find_element_by_xpath(element)
            return True
        except: 
            counter =-1
            time.sleep(0.5)
    return False

##############################################################################
#PARÁMETROS DE DESCARGA
email = 
pwd = 

fechaDesde = '27/11/2022'
fechaHasta = '23/12/2022'
##############################################################################

#ACCESO A LA WEB DEL TPV
#Driver
driver = webdriver.Chrome('C:\\Users\\Nach\\chromedriver.exe')
driver.get("https://restaurant.turbopos.es/login")

#LOGIN
emailInput = driver.find_element_by_xpath("//input[@name  = 'email']")
pwdInput = driver.find_element_by_xpath("//input[@name  = 'password']")

emailInput.clear()
emailInput.send_keys(email)
pwdInput.clear()
pwdInput.send_keys(pwd)

driver.find_element_by_xpath("//button[text()  = 'Login']").click()
##############################################################################
#NAVEGACION
#Click en registro de acciones  
if(wait_element(driver, "//*[@id='bd-main-nav']/ul/li[7]/a/p", 30)):
    print("Se ha encontrado el boton registro de acciones")
      
    driver.find_element_by_xpath("//*[@id='bd-main-nav']/ul/li[7]/a/p").click()
 
time.sleep(5)
#Seleccionar registro de acciones
driver.find_element_by_xpath("//*[@id='bd-main-nav']/ul/li[7]/a/p").click()  
time.sleep(5)
#Click en Elegir rango ( de fechas)
driver.find_element_by_xpath("//*[@value='Elegir rango']").click()
time.sleep(3)

#Filtrado de fechas de los datos almacenados
#"formato fechas dd/mm/yyyy"
#input desde
desdeInput = driver.find_element_by_xpath("//*/label[text()  = 'Desde']/following::input[1]")
desdeInput.clear()
desdeInput.send_keys(fechaDesde)

#input hasta
hastaInput = driver.find_element_by_xpath("//*/label[text()  = 'Hasta']/following::input[1]")
hastaInput.clear()
hastaInput.send_keys(fechaHasta)

time.sleep(3)
##############################################################################
#LECTURA DE LOS DATOS DEL TPV
before_XPath = "/html/body/app-root/app-dashboard/div/div[2]/app-logs/div/div/div/div[6]/div/div[2]/table/tbody/tr["
aftertd_XPath = "]/td["
aftertr_XPath = "]"


columnas = ["fecha", "accion", "descripcion", "usuario", "autorizado_por"]
df = pd.DataFrame(columns = columnas)


#bucle para leer los datos
while(len(driver.find_elements_by_xpath("//*/li[@class = 'pagination-next disabled']")) == 0):

    rows = len(driver.find_elements_by_xpath("/html/body/app-root/app-dashboard/div/div[2]/app-logs/div/div/div/div[6]/div/div[2]/table/tbody/tr"))
    columns = len(driver.find_elements_by_xpath("/html/body/app-root/app-dashboard/div/div[2]/app-logs/div/div/div/div[6]/div/div[2]/table/tbody/tr[2]/td"))
    
    for t_row in range(1, (rows + 1)):
        fila = []
        for t_column in range(1, (columns + 1)):
            FinalXPath = before_XPath + str(t_row) + aftertd_XPath + str(t_column) + aftertr_XPath
            cell_text = driver.find_element_by_xpath(FinalXPath).text
            fila.append(cell_text)
            print(str(t_row))
            print(cell_text)
           
        nuevaFila = pd.Series(fila, index=df.columns) # creamos un objeto Seris
        df = df.append(nuevaFila, ignore_index=True)
        
    driver.find_element_by_xpath("//*/li[@class = 'pagination-next']").click()
    time.sleep(0.5)


fechaDesde = fechaDesde.replace('/','')
fechaHasta = fechaHasta.replace('/','')
##############################################################################
#GUARDADO DE LOS DATOS
df.to_csv("C:\\Users\\Nach\\Desktop\\TFM\\datos\\datos_bruto\\datos_bruto" + fechaDesde + "_" + fechaHasta + "_csv.csv")
df.to_excel("C:\\Users\\Nach\\Desktop\\TFM\\datos\\datos_bruto\\datos_bruto" + fechaDesde + "_" + fechaHasta + "_xlsx.xlsx")
